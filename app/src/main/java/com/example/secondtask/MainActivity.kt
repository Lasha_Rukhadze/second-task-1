package com.example.secondtask

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.secondtask.databinding.ActivityMainBinding
import java.lang.Integer.parseInt
import java.util.Timer
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.doneButton.setOnClickListener {
                changeIntoText(it)

            Timer("", false).schedule(4000) {
                val intent = intent
                finish()
                startActivity(intent)
            }
        }
    }

    private
    fun transform (intNum : Int) : String {

        val list1 = mutableListOf(" ", "ერთი" , "ორი", "სამი", "ოთხი", "ხუთი", "ექვსი", "შვიდი", "რვა",
            "ცხრა", "ათი", "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი",
            "თხუთმეტი", "თექვსმეტი", "ჩვიდმეტი", "თვრამეტი", "ცხრამეტი", "ოცი",
            "ოცი","ორმოცი","ორმოცი", "სამოცი", "სამოცი",
            "ოთხმოცი", "ოთხმოცი", "ასი", "ათასი")
        lateinit var str : String
        lateinit var string : String
        val da : String = "და"

        fun lowTransform(num : Int) : String {

            if ((num / 10) < 2){
                return list1[((num / 10).toString() + (num % 10).toString()).toInt()]
            }
            if ((num / 10) > 1 && (num / 10) < 10) {
                str = list1[20 + num/10 - 2].toString()
                if (num % 10 != 0){
                    if ((num/10) % 2 == 0) return str.dropLast(1).plus(da).plus(list1[num % 10])
                    else return str.dropLast(1).plus(da).plus(list1[10 + num % 10])
                }
                else {
                    if ((num/10) % 2 == 0) return str
                    else return str.dropLast(1).plus(da).plus(list1[10])
                }
            }
            return " "
        }
        if (intNum / 1000 > 0) { return list1[29] }

        if(intNum / 100 > 0) {
            if (intNum / 100 == 1) {
                if (intNum % 100 != 0) string = list1[28].dropLast(1)
                else return list1[28]
            }
            else if (intNum / 100 > 1 && intNum % 100 == 0) {
                return list1[intNum/100].dropLast(1).plus(list1[28])
            }
            else {string = list1[intNum/100].dropLast(1).plus(list1[28].dropLast(1))}

            return string.plus(lowTransform(intNum % 100))
        }
        else { string = lowTransform(intNum) }

        return string
    }

    private fun changeIntoText(view: View){
        binding.apply {
            binding.numberText.text = transform(parseInt(binding.number.text.toString()))
            binding.numberText.visibility = View.GONE
            binding.doneButton.visibility = View.GONE
            binding.numberText.visibility = View.VISIBLE
        }
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}